<?php
/**
 * Created by PhpStorm.
 * User: Shamail
 * Date: 12/1/2014
 * Time: 2:19 PM
 */

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../favicon.ico">

    <title>Fixed Top Navbar Example for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../dist/css/navbar-fixed-top.css" rel="stylesheet">


</head>

<body>

<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.html">Virtual Ward Round</a>
            <ul class="nav navbar-nav">
                <li class="active"><a href="../index.html">Home</a></li>
            </ul>

        </div>
        <div id="navbar" class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Reports <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">


                        <li class="dropdown-header">Bi-Weekly reports</li>
                        <li><a href="reportViewer.php?bw1">Report 1</a></li>
                        <li><a href="reportViewer.php?bw2">Report 2</a></li>
                        <li><a href="reportViewer.php?bw3">Report 3</a></li>
                        <li><a href="reportViewer.php?bw4">Report 4</a></li>

                        <li class="divider"></li>
                        <li class="dropdown-header">Other reports</li>
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">

    <!-- Main component for a primary marketing message or call to action -->
    <div class="row">
        <div class="col-lg-12">
            <h1>Report Title</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-10">
            <h2>Section Heading</h2>

            <h3>Sub-section heading</h3>
            <p>Section Content is going to go in here.
            Will be  along peice of text.</p>
            <br><br>

            <h3>2nd Sub-section heading</h3>
            <p>Section Content is going to go in here.
                Will be  along peice of text.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-10">
            <h2>2nd Section Heading</h2>

            <h3>Sub-section heading</h3>
            <p>Section Content is going to go in here.
                Will be  along peice of text.</p>
            <br><br>

            <h3>2nd Sub-section heading</h3>
            <p>Section Content is going to go in here.
                Will be  along peice of text.</p>
        </div>
    </div>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="../dist/js/bootstrap.min.js"></script>

</body>
</html>
