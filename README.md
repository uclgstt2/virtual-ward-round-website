# README #

### What is this repository for? ###

* This repository hosts all the content for the Project Website

### How do I get set up? ###

* Install Git on your machine.
* Clone the repository using the link displayed at the top.

### About the Project ###
* We will use PHP for backend
* And HTML5/Bootsrap/JavaScript for frontend

### Contributors ###
* Sarjeena Maodud
* Merrick Gigg
* Shamail Ahmed

### Contribution guidelines ###

* Practice making changes to the code by using the fork-commit-merge technique
* Basically, before making any changes, you fork repo from bitbucket
* Then you change and commit.
* Then you merge with the master branch
* You can find it in one of the bitbucket tutorials

